<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
    	

    	$nome = '';
    	$array;
    	foreach ($files as $key => $value) {
    		

    		$nome = $nome.' '.$value;

    		//echo '<br>'.$nome;

    	}
    	$nome = explode(" ", $nome);

    	for($i = 1; $i < count($nome); $i++){

    		$n_nome[ucfirst($nome[$i])] = '';

    		if ($i == 1 || $n_nome[ucfirst($nome[$i])] == $nome[$i - 1] ) {
    			$n_nome[ucfirst($nome[$i])] = $nome[$i];
    		}else{
    			$n_nome[ucfirst($nome[$i])] = '' ;
    		}

    	}

    	foreach ($files as $key => $value) {
    		$i = 1;



    		foreach ($n_nome as $key2 => $value2) {

    			

    			if (ucfirst($value) == $key2) {
	    			$n_nome[$key2] = (!empty($value2)) ? $value2.','.$key : $key;
	    		}
    		}
    		
    	}

        return $n_nome;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));

?>